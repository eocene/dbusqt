/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Global.hpp"
#include "ViewsAdaptor.hpp"

#include <wayfire/nonstd/wlroots-full.hpp>

ViewsAdaptor::ViewsAdaptor( QObject *parent ) : QDBusAbstractAdaptor( parent ) {
}


ViewsAdaptor::~ViewsAdaptor() {
}


/** Query the active view - view with cursor focus */
uint ViewsAdaptor::QueryActiveView() {
    auto view = core.get_cursor_focus_view();

    if ( not view ) {
        return 0;
    }

    return view->get_id();
}


/** Close the view @view_id */
void ViewsAdaptor::CloseView( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( view ) {
        wf::wl_idle_call *idle_call = new wf::wl_idle_call;
        idle_call->run_once(
            [ = ] () {
                view->close();
            }
        );
    }
}


/** Focus the view @view_id */
void ViewsAdaptor::FocusView( uint view_id ) {
    wayfire_toplevel_view view = wf::toplevel_cast( get_view_from_id( view_id ) );

    if ( view ) {
        wf::wl_idle_call *idle_call = new wf::wl_idle_call;
        idle_call->run_once(
            [ = ] () {
                core.default_wm->focus_request( view );
            }
        );
    }
}


/** Fullscreen the view @view_id on output @output_id.
 *  If output_id == 0, current output is used */
void ViewsAdaptor::ToggleFullscreenView( uint view_id, uint output_id ) {
    wf::output_t *output = get_output_from_id( output_id );

    if ( not output ) {
        output = core.get_active_output();

        if ( not output ) {
            return;
        }
    }

    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return;
    }

    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            core.default_wm->fullscreen_request( view, output, not view->toplevel()->pending().fullscreen );
        }
    );
}


/** Maximize/Restore the view @view_id */
void ViewsAdaptor::ToggleMaximizeView( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return;
    }

    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            /* Already maximized: restore */
            if ( view->toplevel()->pending().tiled_edges == wf::TILED_EDGES_ALL ) {
                core.default_wm->tile_request( view, 0 );
            }

            else {
                core.default_wm->tile_request( view, wf::TILED_EDGES_ALL );
            }
        }
    );
}


/** Minimize/Restore the view @view_id */
void ViewsAdaptor::ToggleMinimizeView( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return;
    }

    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            view->set_minimized( not view->minimized );
        }
    );
}


/** Stick/Unstick the view @view_id */
void ViewsAdaptor::ToggleStickyView( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return;
    }

    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            view->set_sticky( not view->sticky );
        }
    );
}


/** Query if the view @view_id is active */
bool ViewsAdaptor::QueryViewActive( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    return (view ? view->activated : false);
}


/** Query the app_id of view @view_id */
QString ViewsAdaptor::QueryViewAppId( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return QString();
    }

    return view->get_app_id().c_str();
}


/** Query the app_id of view @view_id set by gtk shell */
QString ViewsAdaptor::QueryViewAppIdGtkShell( uint ) {
    return QString();
}


/** Query the title of view @view_id */
QString ViewsAdaptor::QueryViewTitle( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return QString();
    }

    return view->get_title().c_str();
}


/** Query the pid of view @view_id */
uint ViewsAdaptor::QueryViewPid( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return 0;
    }

    auto main_surface = view->get_wlr_surface();

    if ( not main_surface ) {
        return 0;
    }

    pid_t pid = 0;
    uid_t uid = 0;
    gid_t gid = 0;

    /** Get the view credentials */
    wl_client_get_credentials( view->get_client(), &pid, &uid, &gid );

    /** If we have a xwayland client, then over-ride the PID. */
    wlr_surface *wlr_surface        = view->get_wlr_surface();
    int         is_xwayland_surface = 0;

    is_xwayland_surface = wlr_surface_is_xwayland_surface( wlr_surface );

    if ( is_xwayland_surface ) {
        pid = wlr_xwayland_surface_from_wlr_surface( wlr_surface )->pid;
    }

    return pid;
}


/** Query if the view @view_id demands attention */
bool ViewsAdaptor::QueryViewAttention( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return false;
    }

    return view->has_data( "view-demands-attention" );
}


/** Query if the view @view_id is fullscreen */
bool ViewsAdaptor::QueryViewFullscreen( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    return (view ? view->toplevel()->pending().fullscreen : false);
}


/** Query if the view @view_id is maximzied */
bool ViewsAdaptor::QueryViewMaximized( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    return (view ? view->toplevel()->pending().tiled_edges == wf::TILED_EDGES_ALL : false);
}


/** Query if the view @view_id minimized */
bool ViewsAdaptor::QueryViewMinimized( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    return (view ? view->minimized : false);
}


/** Query if the view @view_id is sticky */
bool ViewsAdaptor::QueryViewSticky( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    return (view ? view->sticky : false);
}


/** Query the output of view @view_id */
uint ViewsAdaptor::QueryViewOutput( uint view_id ) {
    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return 0;
    }

    wf::output_t *output = view->get_output();

    if ( not output ) {
        return 0;
    }

    return output->get_id();
}
