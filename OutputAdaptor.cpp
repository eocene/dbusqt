/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Global.hpp"
#include "OutputAdaptor.hpp"

#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

OutputAdaptor::OutputAdaptor( QObject *parent ) : QDBusAbstractAdaptor( parent ) {
    setAutoRelaySignals( true );
}


OutputAdaptor::~OutputAdaptor() {
}


/** Show the desktop of the current workspace of the current output */
void OutputAdaptor::ShowDesktop() {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    mShowingDesktop = not mShowingDesktop;

    idle_call->run_once(
        [ = ] () {
            // wf::output_t *output = core.get_active_output();

            /** We jsut flipped to showing the desktop. Do the action. */
            // if ( mShowingDesktop ) {
            //     wf::point_t _ws = output->workspace->get_current_workspace();
            //     QList<wayfire_view> curWsViews;
            //
            //     for ( wayfire_view view: output->workspace->get_views_in_layer( wf::WM_LAYERS ) ) {
            //         if ( view->get_output()->get_id() == output->get_id() ) {
            //             if ( not view->is_mapped() ) {
            //                 continue;
            //             }
            //
            //             if ( not output->workspace->view_visible_on( view, _ws ) ) {
            //                 continue;
            //             }
            //
            //             if ( view->activated ) {
            //                 view->store_data( std::make_unique<wf::custom_data_t>(),
            // "dbusqt-showdesktop-focus" );
            //             }
            //
            //             curWsViews << view;
            //         }
            //     }
            //
            //     for ( wayfire_view view: curWsViews ) {
            //         if ( view->minimized ) {
            //             continue;
            //         }
            //
            //         view->minimize_request( true );
            //         view->store_data( std::make_unique<wf::custom_data_t>(), "dbusqt-showdesktop" );
            //     }
            // }
            //
            // /** We jsut flipped to showing the windows. Do the action. */
            // else {
            //     for ( wayfire_view view : output->workspace->get_views_in_layer( wf::ALL_LAYERS, true ) )
            // {
            //         if ( view->has_data( "dbusqt-showdesktop" ) ) {
            //             view->erase_data( "dbusqt-showdesktop" );
            //             view->minimize_request( false );
            //         }
            //     }
            //
            //     for ( wayfire_view view : output->workspace->get_views_in_layer( wf::ALL_LAYERS, true ) )
            // {
            //         if ( view->has_data( "dbusqt-showdesktop-focus" ) ) {
            //             view->erase_data( "dbusqt-showdesktop-focus" );
            //             view->set_activated( true );
            //         }
            //     }
            // }
        }
    );
}


/** List all the connected output IDs */
QUIntList OutputAdaptor::QueryOutputIds() {
    QUIntList outputs;

    for ( wf::output_t *output: wf_outputs ) {
        outputs << output->get_id();
    }

    return outputs;
}


/** Query the currently active output */
uint OutputAdaptor::QueryActiveOutput() {
    uint output_id = core.get_active_output()->get_id();

    return output_id;
}


/** Query the workspace grid size of the output @output_id */
WorkSpaceGrid OutputAdaptor::QueryOutputWorkspaceGrid( uint output_id ) {
    wf::output_t  *output = get_output_from_id( output_id );
    WorkSpaceGrid wsg;

    if ( output ) {
        wf::dimensions_t grid = output->wset()->get_workspace_grid_size();
        wsg = WorkSpaceGrid{ grid.height, grid.width };
    }

    return wsg;
}


/** Query the current workspace of the output @output_id */
WorkSpace OutputAdaptor::QueryOutputWorkspace( uint output_id ) {
    wf::output_t *output = get_output_from_id( output_id );
    WorkSpace    ws;

    if ( output ) {
        wf::point_t curWS = output->wset()->get_current_workspace();
        ws = WorkSpace{ curWS.y, curWS.x };
    }

    return ws;
}


/** Change the workspace of the given output @output_id */
void OutputAdaptor::ChangeWorkspace( uint output_id, WorkSpace ws ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wf::output_t *output = get_output_from_id( output_id );

            if ( output ) {
                output->wset()->request_workspace( { ws.column, ws.row } );
            }

            delete idle_call;
        }
    );
}


/** List all the views on the current output */
QUIntList OutputAdaptor::QueryActiveOutputViews() {
    uint output_id = core.get_active_output()->get_id();

    return QueryOutputViews( output_id );
}


/** List all the views on the output @output_id.
 *  The views should be top-level and mapped */
QUIntList OutputAdaptor::QueryOutputViews( uint output_id ) {
    QUIntList views;

    for ( wayfire_view view: core.get_all_views() ) {
        if ( (view->get_output()->get_id() == output_id)
             and (view->role == wf::VIEW_ROLE_TOPLEVEL)
             and view->is_mapped() ) {
            views << view->get_id();
        }
    }

    return views;
}


/** Present windows of the current workspace of the current output */
void OutputAdaptor::PresentWorkspaceViews( uint output_id ) {
    /** To be replaced with IPC calls */
    Q_UNUSED( output_id );
}


/** Present windows of all workspaces of the current output */
void OutputAdaptor::PresentOutputViews( uint output_id ) {
    /** To be replaced with IPC calls */
    Q_UNUSED( output_id );
}
