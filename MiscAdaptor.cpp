/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Global.hpp"
#include "MiscAdaptor.hpp"

MiscAdaptor::MiscAdaptor( QObject *parent ) : QDBusAbstractAdaptor( parent ) {
    setAutoRelaySignals( true );

    for ( wf::output_t *output : wf_outputs ) {
        output->connect( &onViewMapped );
        output->connect( &onViewMinimized );
        output->connect( &onViewTiled );
        output->connect( &onViewChangeWorkspace );
        output->connect( &onViewFocused );
        output->connect( &onViewStickied );
        output->connect( &onOutputWorkspaceChanged );
        output->connect( &onOutputWorkspaceGridChanged );

        connected_wf_outputs.insert( output );

        QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "OutputAdded" );
        msg << output->get_id();
        QDBusConnection::sessionBus().send( msg );
    }

    for ( wayfire_view view : core.get_all_views() ) {
        view->connect( &onViewAppIdChanged );
        view->connect( &onViewTitleChanged );
        view->connect( &onViewUnmapped );
        view->connect( &onViewTiled );
    }

    core.connect( &onViewHintsChanged );
    core.connect( &onViewWSetChanged );
    core.connect( &onOutputGainFocus );
    core.output_layout->connect( &onOutputAdded );
    core.output_layout->connect( &onOutputRemoved );
}


MiscAdaptor::~MiscAdaptor() {
}


QString MiscAdaptor::GetVersion() {
    return "v1.0.0";
}


void MiscAdaptor::CaptureActiveScreen( QString filename ) {
    qDebug() << "Use x-d-p-wlr or grim to capture active screen";
}


void MiscAdaptor::CaptureActiveView( QString filename ) {
    /** Current output */
    wf::output_t *output = core.get_active_output();

    if ( not output ) {
        return;
    }

    wayfire_view fview = output->get_active_view();

    if ( not fview ) {
        return;
    }

    /** Capture the view */
    CaptureView( fview->get_id(), filename );
}


void MiscAdaptor::CaptureView( uint view_id, QString filename ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wayfire_view fview = get_view_from_id( view_id );

            if ( not fview ) {
                return;
            }

            /* Take the screenshot */
            wf::render_target_t offscreen_buffer;
            fview->take_snapshot( offscreen_buffer );
            auto width  = offscreen_buffer.viewport_width;
            auto height = offscreen_buffer.viewport_height;

            /** Get the pixels */
            GLubyte *pixels = ( GLubyte * )malloc( width * height * sizeof(GLubyte) * 4 );

            if ( not pixels ) {
                return;
            }

            /** Render the pixels into */
            OpenGL::render_begin();
            GL_CALL( glBindFramebuffer( GL_FRAMEBUFFER, offscreen_buffer.fb ) );
            GL_CALL( glViewport( 0, 0, width, height ) );
            GL_CALL( glReadPixels( 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels ) );
            OpenGL::render_end();

            /** Write the image to png file */
            image_io::write_to_file( filename.toUtf8().constData(), pixels, width, height, "png", true );
            free( pixels );

            delete idle_call;
        }
    );
}


void MiscAdaptor::CaptureViewUnderMouse( QString filename ) {
    /* Get the current mouse position */
    wf::pointf_t cursor_pos = core.get_cursor_position();

    /* Get the view under the mouse */
    wayfire_view fview = core.get_view_at( cursor_pos );

    if ( not fview ) {
        return;
    }

    CaptureView( fview->get_id(), filename );
}


void MiscAdaptor::SaveSession( QString name ) {
    Q_UNUSED( name );
}


void MiscAdaptor::RestoreSession( QString name ) {
    Q_UNUSED( name );
}
