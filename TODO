Demands attention is the state where a view is shown but does not have keyboard focus yet even though it can have keyboard focus.
To enable this, we need the following.

1. On view_mapped, set "demands-attention" to the view, and emit "view-hints-changed" signal

    view->store_data( std::make_unique<wf::custom_data_t>(), "demands-attention" );

    wf::view_hints_changed_signal data;
    data.view = view;
    data.demands_attention = true;

    wf::get_core().emit_signal( "view-hints-changed", &data );
    view->emit_signal( "hints-changed", &data );

2. Once it receives keyboard focus, remove the "demands-attention" data, and send the signal again.

    view->erase_data( "demands-attention" );

    wf::view_hints_changed_signal data;
    data.view = view;
    data.demands_attention = false;

    wf::get_core().emit_signal( "view-hints-changed", &data );
    view->emit_signal( "hints-changed", &data );

Eventually, we can map this to Focus view demanding attention keybinding (ex: Ctrl-Alt-A)

3. The get the screenshot of the full screen, checkout output mirroring from output_layout.cpp
